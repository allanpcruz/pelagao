# Documentação do Projeto "Pelagão Universitário"

## Informações Importantes
O "Pelagão Universitário" encontra-se descontinuado e não receberá mais atualizações, pois foi substituído pelo "Pelego React", que é uma versão mais avançada e moderna do gerador de frases aleatórias. O "Pelego React" está disponível em https://pelagao.vercel.app/. Recomendo o uso deste projeto em caso de interesse em introdução ao desenvolvimento web sem frameworks.
O código-fonte do "Pelagão Universitário" está disponível em https://gitlab.com/allanpcruz/pelego-react

## Visão Geral
O "Pelagão Universitário" é um projeto de gerador de frases aleatórias voltado para simular discursos comunistas de universidade. O objetivo do projeto é fornecer uma ferramenta divertida e educativa para criar frases com um tom revolucionário.

O projeto consiste em uma página da web simples que exibe uma frase aleatória gerada a partir de várias partes de discurso, como saudações, demandas, críticas e slogans revolucionários.

O projeto está hospedado em https://pelagao.vercel.app/.

## Funcionalidades
* Geração de Frases Aleatórias: O projeto permite gerar frases aleatórias com base em diferentes partes de discurso, como saudações, demandas e slogans revolucionários.
* Exibição Instantânea: Ao abrir a página, uma frase aleatória é exibida automaticamente, proporcionando uma experiência imediata ao usuário.
* Interface Simples: A interface do usuário é simples e intuitiva, consistindo em uma área de exibição de frase e um botão para gerar uma nova frase.

## Tecnologias Utilizadas

* HTML: Utilizado para criar a estrutura básica da página da web.
* CSS: Utilizado para estilizar a página e torná-la visualmente atraente.
* JavaScript: Utilizado para gerar frases aleatórias e manipular o conteúdo da página dinamicamente.
* Vercel: Utilizado para hospedar o projeto e disponibilizá-lo na web.

## Como Usar
1. Acesse https://pelagao.vercel.app/ em um navegador da web.
2. Ao abrir a página, uma frase aleatória será exibida automaticamente na área designada.
3. Se desejar gerar uma nova frase, clique no botão "Gerar".
4. Uma nova frase aleatória será gerada e exibida na área designada.

## Autores
Allan Possani da Cruz (allanpcruz@gmail.com) - Desenvolvedor Principal

## Licença
Este projeto está licenciado sob a Licença BSD de 3 Cláusulas - veja o arquivo [LICENSE](LICENSE) para mais detalhes.
