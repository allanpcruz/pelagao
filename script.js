document.addEventListener('DOMContentLoaded', gerarFrase);
document.querySelector('#gerar-frase').addEventListener('click', gerarFrase);

function gerarFrase() {
    const coluna1 = [
        'Caros colegas, ',
        'Companheiros e companheiras, ',
        'Amigos e amigas,',
        'Como temos lutado, ',
        'Camaradas, ',
        'Caros amigos, ',
        'Caros militantes do povo, ',
        'Companheiros estudantes, ',
        'Companheiros de luta, ',
        'Juventude revolucionária, '
    ];

    const coluna2 = [
        'é necessário mobilizar a greve geral ',
        'precisamos organizar um boicote ',
        'chegou a hora de participar da manifestação ',
        'queremos uma revolução ',
        'estamos organizando uma grande passeata ',
        'é preciso se mobilizar ',
        'é hora de um grande ato público combativo pela luta popular classissista ',
        'estamos manifestando nosso total apoio à invasão em protesto ',
        'vamos organizar uma marcha ',
        'é preciso criar condições de luta '
    ];

    const coluna3 = [
        'contra a elite branca reacionária e suas reformas neoliberais ',
        'contra o capitalismo consumista gerador da poluição e da miséria ',
        'pela dos Estados Unidos e integração da américa latina ',
        'contra o imperialismo burguês e pelo bolivarianismo indígena ',
        'contra a globalização e pela ruptura com o modelo econômico e social vigente da civilização judaico-cristã ocidental ',
        'contra a política fascista peleguista do PSDB/DEM ',
        'contra a Rede Globo, Veja e Folha de São Paulo, que atendem aos interesses ianques e do FMI ',
        'contra a mídia burguesa a serviço do capital ',
        'contra o Estado policial opressor e pela descriminalização das drogas ',
        'contra as grandes corporações '
    ];

    const coluna4 = [
        'para alcançar a justiça social.',
        'de forma a se atender a função social da terra.',
        'para atender as necessidades sociais dos excluídos.',
        'de forma que possamos derrotar o projeto da ALCA.',
        'porque só assim teremos uma universidade pública gratuíta e de qualidade.',
        'porque o povo almeja maior participação.',
        'para alcançar os objetivos propostos pelo mestre Karl Marx',
        'para derrotar as reformas neoliberais.',
        'e romper com os interesses empresariais de lucro.',
        'e atender aos interesses do proletariado'
    ];

    const grito = [
        'Abaixo o fascismo burguês!',
        'Hasta la victoria!',
        'No pasaran!',
        'Morte ao sistema!',
        'Viva Che! Viva Fidel!',
        'Morte ao imperialismo',
        'Greve geral já!',
        'Abaixo as opressões!',
        'Todo poder aos estudantes!',
        'Morte aos EUA e Israel!'
    ];

    const parte1 = coluna1[Math.floor(Math.random() * coluna1.length)];
    const parte2 = coluna2[Math.floor(Math.random() * coluna2.length)];
    const parte3 = coluna3[Math.floor(Math.random() * coluna3.length)];
    const parte4 = coluna4[Math.floor(Math.random() * coluna4.length)];
    const gritoAleatorio = grito[Math.floor(Math.random() * grito.length)];

    const frase = `${parte1} ${parte2} ${parte3} ${parte4}`;

    document.querySelector('.pelagao').innerText = frase;
    document.querySelector('.grito').innerText = gritoAleatorio;

}
